<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>Algorithm:</source>
        <translation>Algoritmo:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Calc</source>
        <translation>Calcular</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Path:</source>
        <translation>Caminho:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Check:</source>
        <translation>Verificação:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Compare the hash</source>
        <translation>Comparar hash</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>Copyright 2017-2020 DanSoft.</source>
        <translation>Copyright 2017-2020 DanSoft.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>All rights reserved.</source>
        <translation>Todos os direitos reservados.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>Open file</source>
        <translation>Abrir arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>All Files (*)</source>
        <translation>Todos os Arquivos (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>equally</source>
        <translation>igual</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>different</source>
        <translation>diferente</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Все файлы (*.*)</translation>
    </message>
</context>
<context>
    <name>ObjectConsole</name>
    <message>
        <location filename="../ObjectConsole.cpp" line="22"/>
        <source>Invalid positional arguments</source>
        <translation>Argumentos posicionais inválidos</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="53"/>
        <source>Invalid hash argument</source>
        <translation>Argumento de hash inválido</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="57"/>
        <source>Filename: %s</source>
        <translation>Arquivo: %s</translation>
    </message>
    <message>
        <source>Result: </source>
        <translation type="vanished">Результат: </translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>Check: %s</source>
        <translation>Verificação: %s</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <source>equally</source>
        <translation>igual</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>different</source>
        <translation>diferente</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="87"/>
        <source>Result: %s</source>
        <translation>Resultado: %s</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="54"/>
        <source>Source file to hash</source>
        <oldsource>Source file to hash.</oldsource>
        <translation>Arquivo origem para hash</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>Hash algorithm</source>
        <oldsource>Hash algorithm.</oldsource>
        <translation>Algoritmo de hash</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="58"/>
        <source>hash comparison</source>
        <translation>Hash para comparação</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="59"/>
        <source>hash</source>
        <translation>hash</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Show progress</source>
        <translation>Mostrar progresso</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Show list all hash algorithm</source>
        <oldsource>Show list all hash algorithm.</oldsource>
        <translation>Mostra lista de todos os algoritmos de hash</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Open in gui</source>
        <translation>Abrir na GUI</translation>
    </message>
    <message>
        <source>Result: </source>
        <translation type="obsolete">Результат: </translation>
    </message>
</context>
</TS>
