<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Close</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>Algorithm:</source>
        <translation>Алгоритм:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Calc</source>
        <translation>Посчитать</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Path:</source>
        <translation>Путь:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Check:</source>
        <translation>Проверка:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Compare the hash</source>
        <translation>Сравнение с хешем</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>Copyright 2017-2020 DanSoft.</source>
        <translation>Copyright 2017-2020 DanSoft.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>All rights reserved.</source>
        <translation>Все права защищены</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>equally</source>
        <translation>одинаковый</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>different</source>
        <translation>разный</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Все файлы (*.*)</translation>
    </message>
</context>
<context>
    <name>ObjectConsole</name>
    <message>
        <location filename="../ObjectConsole.cpp" line="22"/>
        <source>Invalid positional arguments</source>
        <translation>Не верно указаны аргументы</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="53"/>
        <source>Invalid hash argument</source>
        <translation>Указан несуществующий hash алгоритм</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="57"/>
        <source>Filename: %s</source>
        <translation>Имя файла: %s</translation>
    </message>
    <message>
        <source>Result: </source>
        <translation type="vanished">Результат: </translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>Check: %s</source>
        <translation>Проверка: %s</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <source>equally</source>
        <translation>одинаковый</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>different</source>
        <translation>разный</translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="87"/>
        <source>Result: %s</source>
        <translation>Результат: %s</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="54"/>
        <source>Source file to hash</source>
        <oldsource>Source file to hash.</oldsource>
        <translation>Исходный файл для хеширования</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>Hash algorithm</source>
        <oldsource>Hash algorithm.</oldsource>
        <translation>Хеш-алгоритм</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="58"/>
        <source>hash comparison</source>
        <translation>Хеш для сравнения</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="59"/>
        <source>hash</source>
        <translation>Хеш</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Show progress</source>
        <translation>Показывать процесс</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Show list all hash algorithm</source>
        <oldsource>Show list all hash algorithm.</oldsource>
        <translation>Вывести все хеш-алгоритмы</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Open in gui</source>
        <translation>Открыть в GUI</translation>
    </message>
    <message>
        <source>Result: </source>
        <translation type="obsolete">Результат: </translation>
    </message>
</context>
</TS>
