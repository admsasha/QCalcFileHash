<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>Algorithm:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Check:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Compare the hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>Copyright 2017-2020 DanSoft.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="24"/>
        <source>All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>different</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ObjectConsole</name>
    <message>
        <location filename="../ObjectConsole.cpp" line="22"/>
        <source>Invalid positional arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="53"/>
        <source>Invalid hash argument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="57"/>
        <source>Filename: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="87"/>
        <source>Result: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>Check: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="91"/>
        <source>equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="93"/>
        <source>different</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="54"/>
        <source>Source file to hash</source>
        <oldsource>Source file to hash.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>Hash algorithm</source>
        <oldsource>Hash algorithm.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="58"/>
        <source>hash comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="59"/>
        <source>hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Show progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Show list all hash algorithm</source>
        <oldsource>Show list all hash algorithm.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Open in gui</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
